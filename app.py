from utils import Results
from metrics import Metrics
from midi import MonophonicTranscription
from file import File
from visualizations.midi import save_spectrogram, plot_both_midi_representations

if __name__ == "__main__":

  f = File('corpus', 'dataset')
  f.evaluate_all_intervals()

  transcribe = [30, 6, 8]

  transcription_groups = [
    [30, 6, 8], #0
    [23, 24, 47], #1
    [10, 16, 17], #2
    [20, 27, 31], #3 #pyin #crepe
    [27, 34, 135], #4
    [40, 48, 58], #5
    [62, 64, 72], #6
    [75, 82, 84], #7
    [87, 90, 102], #8
    [103, 104, 107], #9
    [111, 23, 24], #10
    [47, 63, 79], #11
    [96, 110, 115], #12
  ]

  method = "crepe"
  _metrics = []

  for (i, v) in enumerate(transcription_groups[:3]):
    transcribe = transcription_groups[i]

    for value in transcribe:
      mt = MonophonicTranscription(method, f"dataset/corpus/{value}.wav", "mode")

      save_spectrogram(mt.audio, f"results/{value}_spectrogram.png")

      m = Metrics(
          _name = f"{str(value)}",
          _method = method,
          _original_notes = f.melodies[str(value)],
          _predicted_notes = mt.notes)

      _metrics.append(m)

  r = Results(_metrics, "b")

  r.export("results")
