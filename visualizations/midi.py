from matplotlib import pyplot as plt
import librosa
import librosa.display
import numpy as np


def save_spectrogram(audio, path: str):
  x = audio.signal
  sr = audio.sr

  bins_per_octave = 36
  cqt = librosa.cqt(x, sr, n_bins = 300, bins_per_octave = bins_per_octave)
  log_cqt = librosa.amplitude_to_db(np.abs(cqt))

  librosa.display.specshow(log_cqt, sr = sr, x_axis = 'time', y_axis='cqt_note', bins_per_octave = bins_per_octave)

  plt.savefig(path)

def plot_midi_representation(intervals: list):
  """
    Given the music intervals, shows the midi notes in rectangular view
  """

  xmax = intervals[-1]['end']

  fig = plt.figure()

  ax = fig.add_subplot(1, 1, 1)

  #plt.axes(ylim=(0, 127), xlim=(0, xmax))
  ax.set_xlim([0, xmax])
  ax.set_ylim([0, 127])

  for interval in intervals:
    rect = plt.Rectangle((interval['begin'], interval['note'] - 0.5), interval['end']-interval['begin'], 1, alpha=0.5)
    ax.add_patch(rect)

  ax.set_ylabel('Nota Midi [0-127]')
  ax.set_xlabel('Tempo (s)')
  ax.set_title('Visualização de um contéudo musical em formato midi')

  plt.show()

def plot_both_midi_representations(_name: str, _color: str, method: str, intervals_original: list, intervals_predicted: list):
  xmax = intervals_original[-1]['end']

  fig = plt.figure()

  ax = fig.add_subplot(1, 1, 1)

  #plt.axes(ylim=(0, 127), xlim=(0, xmax))
  ax.set_xlim([0, xmax])
  ax.set_ylim([0, 127])

  #handle
  handle = {}

  color = 'red'
  for interval in intervals_original:
    rect = plt.Rectangle((interval['begin'], interval['note'] - 0.5), interval['end']-interval['begin'], 1, alpha=0.5, color=color, facecolor=color)
    handle[color] = ax.add_patch(rect)

  color = _color
  for interval in intervals_predicted:
    rect2 = plt.Rectangle((interval['begin'], interval['note'] - 0.25), interval['end']-interval['begin'], 0.5, alpha=0.5, color=color, facecolor=color)
    handle[color] = ax.add_patch(rect2)

  plt.legend([handle['red'], handle[_color]], ['Original', method])
  plt.savefig(f"{_name}_{method}.png")
  plt.show()

def plot_three_midi_representations(original: list, pyin_intervals: list, crepe_intervals: list):
  xmax = original[-1]['end']

  fig = plt.figure()

  ax = fig.add_subplot(1, 1, 1)

  #plt.axes(ylim=(0, 127), xlim=(0, xmax))
  ax.set_xlim([0, xmax])
  ax.set_ylim([0, 127])

  for interval in original:
    rect = plt.Rectangle((interval['begin'], interval['note'] - 0.5), interval['end']-interval['begin'], 1, alpha=0.5, color='r')
    ax.add_patch(rect)

  ax.set_label("Original")

  for interval in pyin_intervals:
    rect2 = plt.Rectangle((interval['begin'], interval['note'] - 0.25), interval['end']-interval['begin'], 0.5, alpha=0.5, color='b')
    ax.add_patch(rect2)

  ax.set_label("Pyin")

  for interval in crepe_intervals:
    rect3 = plt.Rectangle((interval['begin'], interval['note'] - 0.25), interval['end']-interval['begin'], 0.5, alpha=0.5, color='g')
    ax.add_patch(rect3)

  ax.set_label("Crepe")

  ax.legend(loc='upper right')

  plt.show()
