from visualizations.midi import save_spectrogram, plot_both_midi_representations
from utils import Results

if __name__== "__main__":

  r = Results(None, None)

  _pred, _or = r.load_notes_from_file('backup/group4/24_pyin_prediction.csv')

  # purple CREPE
  # green Pyin
  plot_both_midi_representations('24', 'green', 'PYin', _or, _pred)
