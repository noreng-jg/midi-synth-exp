from visualizations.midi import save_spectrogram, plot_midi_representation
from utils import Results

if __name__== "__main__":

  r = Results(None, None)

  _, _or = r.load_notes_from_file('backup/group4/135_crepe_prediction.csv')

  plot_midi_representation(_or)
