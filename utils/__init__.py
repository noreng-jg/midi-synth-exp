import os

class Results:
  def __init__(self, metrics, audio):
    self.metrics = metrics
    self.audio = audio

  def load_notes_from_file(self, path: str):
    os.stat(path)

    predictions = []
    originals = []

    with open(path) as f:
      lines = f.readlines()[1:]
 
      for line in lines:
        values = line.split(',') 

        _pr = {
          'begin': float(values[1]),
          'end': float(values[2]),
          'note': int(values[3]) 
        }

        _or = {
          'begin': float(values[4]),
          'end': float(values[5]),
          'note': int(values[-1][:-1])
        }

        predictions.append(_pr)
        originals.append(_or)

    return (predictions, originals)

  def export(self, path: str):
    """
    Parameters:

    -> Array of metrics

    0.  Export the png content of the spectrogram

    Use the audio for that
    ______________________________________

    1. export the transcribed notes of each predicted sound for
    each transcription

    # Example
    # results/19_crepe_prediction.csv
    # results/19_pyin_prediction.csv


    Should look like:
    __________________________________

    t_i | p_begin | p_end | p_note | o_begin | o_end | o_note
    _____

    0    |
    1    |
    2    |
    ...
    n    |
    __________________________________

    # Which fields would there be?

    # 2. export the metrics of the transcription

    Example: results/metrics_crepe.csv
            ; results/metrics_pyin.csv

    (Will use an array of transcriptions)

    metrics | T_LEN | CTN | PTN | FER | OER | MIN | FAN | NDA
    ________

    t_0     |
    t_1     |
    t_2     |
    ...
    t_n     |
    __________________________________

    # export the png of the comparison of the notes transcriptions (CREPE and PYIN)
    [For that you can use the exported results, so there is no need for that]

    (Show the legend)

   """

    for (_i, metric) in enumerate(self.metrics):
      with open(f"{path}/metrics_{metric.method}.csv", 'a') as _m:
        if _i == 0:
          _m.writelines('metrics,T_LEN,CTN,PTN,OER,MIN,FAN,FER,NDA\n')

        _m.writelines(f"t_{metric.name},{len(metric.predicted_notes)},{metric.CTNS},{metric.PTNS},{metric.OERS},{metric.MINS},{metric.FANS},{metric.FERS},{metric.NDA}\n")

      for (_j, transcription) in enumerate(metric.predicted_notes):
        with open(f"{path}/{metric.name}_{metric.method}_prediction.csv", 'a') as _t:
          if _j == 0:
            _t.writelines(f"t_i,p_begin,p_end,p_note,o_begin,o_end,o_note\n")
          _t.writelines(f"{_j},{transcription['begin']},{transcription['end']},{transcription['note']},{metric.original_notes[_j]['begin']},{metric.original_notes[_j]['end']},{metric.original_notes[_j]['note']}\n")

      _t.close()
      _m.close()
            

      print(metric.predicted_notes)
      pass
