from math import nan, isnan, floor, ceil

class Metrics:
  predicted_notes = []
  original_notes = []

  PARTIAL_LEFT = "partial left"
  PARTIAL_RIGHT = "partial right"
  OVERLAP = "overlap"
  INSIDE = "inside"

  CTNS = 0
  PTNS = 0
  FERS = 0
  OERS = 0
  MINS = 0
  FANS = 0

  NDA = None

  def evaluate_note_location(self, _o_n, _p_n):
    if _p_n['begin'] >= _o_n['begin'] and _p_n['begin'] <= _o_n['end']: # inside
      print("inside")
      return self.INSIDE

    elif _o_n['begin'] >= _p_n['begin'] and _o_n['end'] > _p_n['end']: #partial left - b case
      print("partial left")
      return self.PARTIAL_LEFT

    elif _o_n['begin'] < _p_n['begin'] and _o_n['end'] <= _p_n['end']: # partial right
      print("partial right")
      return self.PARTIAL_RIGHT

    elif _p_n['begin'] < _o_n['begin'] and _p_n['end'] > _o_n['end']: # overlap
      print("overlap")
      return self.OVERLAP

    elif _p_n['begin'] < _o_n['begin']:
      print("partial left")
      return self.PARTIAL_LEFT

    else:
      print("partial right")
      return self.PARTIAL_RIGHT

  def __init__(self, _name: str, _method: str, _original_notes: list, _predicted_notes: list):
    """
      _name: file_name
      _method: algorithm used for the transcription

    """

    self.predicted_notes = _predicted_notes
    self.original_notes = _original_notes
    self.name = _name
    self.method = _method

    self.process_content()

  def process_content(self):
    # check array sizes

    if len(self.original_notes) != len(self.predicted_notes):
      if len(self.predicted_notes) == len(self.origial_notes) + 1:
        # join the first two notes

        join = {
          'note': self.predicted_notes[0]['note'],
          'begin': self.predicted_notes[0]['begin'],
          'end': seld.predicted_notes[1]['end']
        } 

        self.predicted_notes = self.predicted_notes[2:]

        self.predicted_notes.insert(0, join)

      else:
        raise Exception("Can't process the content, the vectors have different sizes.")

     # round the midi note values

    for (i, v) in enumerate(self.original_notes):
       predicted_note_value = self.predicted_notes[i]['note']
       original_note_value = self.original_notes[i]['note']

       if (predicted_note_value - floor(predicted_note_value) > .5):
         self.predicted_notes[i]['note'] = ceil(predicted_note_value)
       else:
         self.predicted_notes[i]['note'] = floor(predicted_note_value)

       if (original_note_value - floor(original_note_value) > .5):
         self.original_notes[i]['note'] = ceil(original_note_value)
       else:
         self.original_notes[i]['note'] = floor(original_note_value)


     # classify note based criteria for all notes of the song

    for (i, v) in enumerate(self.original_notes):
      self.classify_note_based_criteria(v, self.predicted_notes[i])

    # evalate note detection accuracy

    self.NDA = (self.CTNS + .5*self.PTNS -2*self.OERS - self.FANS) / len(self.original_notes)


  def _ROT(self, _o_n, _p_n):

    """
      REFERENCE OVERLAPPING TRANSCRIPTION

      - OVER Transcripition -> Predicted range as denominator

      ==================================

      Indicatas which percentage of the transcribed duration intersects the reference

    """

    location = self.evaluate_note_location(_o_n, _p_n)

    _delta_x = None

    if location == self.PARTIAL_LEFT or location == self.PARTIAL_RIGHT:
      if location == self.PARTIAL_LEFT:
        _delta_x = _p_n['end'] - _o_n['begin']

      else:
        _delta_x = _p_n['begin'] - _o_n['end']

    elif location == self.OVERLAP:
      _delta_x = _o_n['end'] - _o_n['begin']

    else:
      _delta_x = _p_n['end'] - _p_n['begin']


    return _delta_x / (_p_n['end'] - _p_n['begin'])

  def _TOR(self, _o_n, _p_n):
    """
      TRANSCRIPTION OVERLAPPING REFERENCE

      - OVER Reference -> Original range as denominator

      ===================================

      Indicates which percentage of the trascribed duration is completely inside the reference

    """

    location = self.evaluate_note_location(_o_n, _p_n)

    _delta_x = None

    if location == self.PARTIAL_LEFT or location == self.PARTIAL_RIGHT:
      if location == self.PARTIAL_LEFT:
        _delta_x = _p_n['end'] - _o_n['begin']

      else:
        _delta_x = _p_n['begin'] - _o_n['end']

    elif location == self.OVERLAP:
      _delta_x = _o_n['end'] - _o_n['begin']

    else:
      _delta_x = _p_n['end'] - _p_n['begin']

    return _delta_x / (_o_n['end'] - _o_n['begin'])

  def evaluate_overlapping_measures(self, _o_n, _p_n):
    """
      Parameters:

        _o_n (list): original note
        _p_n (list): predicted note

      Outputs:

        _dictionary

    """

    ROT = self._ROT(_o_n, _p_n)
    TOR = self._TOR(_o_n, _p_n)

    overlapping_measures =  {
      'ROT': ROT,
      'TOR': TOR
    }

    print(overlapping_measures)


    return overlapping_measures
    #_ROT

    # fazer um estudo de caso

    # 1. parcialmente contida
    # 2. inteiramente contida
    # 3. exceder a original 

    pass

  def classify_note_based_criteria(self, _o_n, _p_n):
    """
      Parameters:

        _o_n (list): original note
        _p_n (list): predicted note

      Outputs:

        _dictionary

      -------------------------------------

      FANS might also occur when a trascribed note does not have a reference note
      to compare. In this study, we won't cover this case


    """
    measures = self.evaluate_overlapping_measures(_o_n, _p_n)

    _ROT = measures['ROT']
    _TOR = measures['TOR']

    time_overlap = False

    if (_ROT + _TOR > 1.5) and (_ROT >= .6) and (_TOR >= .6):
      if _o_n['note'] == _p_n['note']:
        self.CTNS += 1

      time_overlap = True

    elif (_TOR + _ROT >= 1 ) and (_TOR >= .4 ):
      if _o_n['note'] == _p_n['note']:
        self.PTNS += 1

      time_overlap = True

    if not time_overlap:
      self.FANS += 1
      self.MINS += 1

    elif _o_n['note'] != _p_n['note']: # is error
      # is octave error
      if isnan(_p_n['note']):
        self.FANS += 1
        self.MINS += 1
      if abs(_o_n['note'] - _p_n['note'] >= 12):
        self.OERS += 1
      else:
        self.FERS += 1
       

if __name__ == "__main__":
  metrics = Metrics([],[])

  metrics.evaluate_note_location(
    _o_n = {
      'begin': 100,
      'end': 200,
    },
    _p_n = {
      'begin': 85,
      'end': 120,
    }
  )

  metrics.evaluate_note_location(
    _p_n = {
      'begin': 50,    
      'end': 250,
    },
    _o_n = {
      'begin': 100,
      'end': 200
    }
  )

  metrics.evaluate_note_location(
    _p_n = {
      'begin': 99,    
      'end': 200,
    },
    _o_n = {
      'begin': 100,
      'end': 200
    }
  )

  metrics.evaluate_overlapping_measures(
      _p_n = {
        'begin': 50,
        'end': 250,
      },
      _o_n = {
        'begin': 100,
        'end': 200,
      }
  )

  metrics.evaluate_overlapping_measures(
      _p_n = {
        'begin': 75,
        'end': 125,
      },
      _o_n = {
        'begin': 100,
        'end': 200,
      }
  )

  metrics.evaluate_overlapping_measures(
      _p_n = {
        'begin': 100,
        'end': 150,
      },
      _o_n = {
        'begin': 100,
        'end': 200,
      }
  )

  metrics.evaluate_overlapping_measures(
      _p_n = {
        'begin': 50,
        'end': 150,
      },
      _o_n = {
        'begin': 100,
        'end': 200,
      }
  )
