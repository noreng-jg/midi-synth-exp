from file import File
from visualizations.midi import *
from midi import MonophonicTranscription

if __name__ == "__main__":
  f = File('corpus', 'dataset')
  f.evaluate_all_intervals()

  m_number = 135

  mt = MonophonicTranscription("pyin", f"dataset/corpus/{m_number}.wav", "mode")
  mt2 = MonophonicTranscription("crepe", f"dataset/corpus/{m_number}.wav", "mean")

  plot_both_midi_representations(f.melodies[str(m_number)], mt.notes)
