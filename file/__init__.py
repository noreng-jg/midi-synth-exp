import os

class File:
  # this factor is an empyrical constante that appproximately converts the midi bin time to seconds representation
  time_factor = 2.15377
  def __init__(self, _type: str, path: str=""):
    """
      File contructor, might vary according to the grouping selected
    """
    if _type not in ['corpus', 'test']:
      raise Exception('The file does not belong to a group')

    is_csv = lambda file: file[-3:] == 'csv'

    if path != "":
      self.path = f"dataset/{_type}"
    else:
      self.path = f"../dataset/{_type}"

    try:
      if _type == 'corpus':
        self.files = os.listdir(self.path)
        self.csv_files = [file for file in filter(is_csv, self.files)]

      elif _type == 'test':
        self.files = os.listdir(self.path)
        self.csv_files = [file for file in filter(is_csv, self.files)]


    except Exception as e:
      print(e)

  def read_midi_content(self):
    """
      Loads the content of the file in a dictionary where the key
      corresponds to the filename
    """

    midi_content = {}

    for file in self.csv_files:
      with open(self.path + '/' + file) as f:
        content = f.readlines()
        midi_content[file[:-4]] = content
        f.close()

    self.midi_content = midi_content

  def get_intervals_from_content(self, key: str):
    """
      Process a specific csv midi file content information in order to retrieve
      the interval information from a given note from start to end in an array of dictionaries

      intervals = [
        {
          'begin':,
          'end': ,
          'note': ,
        }
      ]

    """
    if not key in self.midi_content.keys():
      raise Exception("Couldn't locate the file")

    content = self.midi_content[key]

    is_note_on_line = lambda line : 'Note_on_c' in line

    note_lines = [note_line.split(',') for note_line in filter(is_note_on_line, content)]

    # 1440, 480
    _delta_time = 960

    intervals = [
      {
        'begin': (float(note_lines[index:index+2][0][1]))/_delta_time, #in seconds
        'end': (float(note_lines[index:index+2][1][1]))/_delta_time,
        'note': int(note_lines[index:index+2][0][4])
      }
    for index in range(0, len(note_lines), 2) ]

    self.intervals = intervals

    return intervals

  def evaluate_all_intervals(self):
    """
      Starts reading all midi content from the csv files and performs
      data processing to each one in order to evaluate the interval metrics
      for each note duration

      Evaluates all interval information to all midi file content in a dictionary
      where the key corresponds to the specific file being adressed

    """

    melodies = {}

    self.read_midi_content()

    for key_content in self.midi_content.keys():
      intervals = self.get_intervals_from_content(key_content)

      melodies[key_content] = intervals

    self.melodies = melodies


if __name__ == "__main__":
  f = File('corpus')

  print(f.csv_files)
  f.evaluate_all_intervals()
  print(f.all_midi_intervals['150'])
  #print(f.midi_content)
  #f.get_intervals_from_content("1")
