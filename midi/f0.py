import crepe
import librosa
import numpy as np
from scipy import stats
from math import nan, isnan
from .meter_estimation import MeterEstimation

class TemporalMerge:
  def evaluate_indices_from_groupings(self, groupings: list):
    _intervals = []
    index = 0
    _current_time = self.times[index]

    for (_i, _grouping) in enumerate(groupings):
      _end = _grouping[1]
      _interval = []

      while _current_time < _end:
        _interval.append(index) 

        if index == len(self.times) - 1:
          break
          
        index += 1
        _current_time = self.times[index]

      _intervals.append(_interval)
  
    return _intervals

  def raw_temporal_merge(self, groupings: list, merge_metric = "mode"):
    """
      find the indices from the grouping intervals
      which times belongs to each groupings

      output should first be a list of indices
    """
    
    if not merge_metric in ["mode", "mean"]:
      raise Exception("Invalid metric")

    time_indices = self.evaluate_indices_from_groupings(groupings)

    _final = []

    for (_i, grouping) in enumerate(groupings):

      _idxs = (time_indices[_i][0], time_indices[_i][-1])

      f0_grouping = self.f0[_idxs[0]:_idxs[1]+1]

      filtered_frequencies = [f for f in filter(lambda _f: not isnan(_f), f0_grouping)]

      if len(filtered_frequencies) == 0:
        _mode = 2000 # ensure that a value exists
      else:
        _mode = stats.mode(filtered_frequencies).mode[0]

      _mean = np.mean(f0_grouping)

      _metric = None

      if merge_metric == "mode":
        _metric = _mode
      else:
        _metric =_mean

      _final.append(
          {
            'begin': grouping[0],
            'end': grouping[1],
            'note': librosa.hz_to_midi(_metric)
          }
      )

    return _final

class Crepe(TemporalMerge):
  def __init__(self, audio):
    self.signal = audio.signal
    self.sr = audio.sr

  def detect_f0s(self):
    print("Detector from Crepe")
    times, f0, confidence, activation = crepe.predict(self.signal, sr=self.sr, viterbi=True, step_size=23)
    self.f0 = f0
    self.times = times

class PYin(TemporalMerge):
  def __init__(self, audio):
    self.signal = audio.signal
    self.sr = audio.sr
      
  def detect_f0s(self):
    print("Detector from Pyin")

    f0, voiced_flags, voiced_probs = librosa.pyin(
        self.signal,
        fmin = librosa.note_to_hz('C2'),
        fmax = librosa.note_to_hz('C8')
    )

    self.f0 = f0
    self.voiced_flags = voiced_flags
    self.voiced_probs = voiced_probs
    self.times = librosa.times_like(f0)


class F0Estimation:
  detector = None

  def __init__(self, method, audio):
    if method not in ["pyin", "crepe"]:
      raise ValueError("The specified method: {method} is not implemented".format(method=method))

    if method == "crepe":
      self.detector = Crepe(audio) 
      
    if method == "pyin":
      self.detector = PYin(audio)

  def detect_f0s(self):
    self.detector.detect_f0s()


if __name__ == "__main__":

  audio = Audio("dataset/corpus/6.wav")
  f0 = F0Estimation("pyin", audio)
  me = MeterEstimation(audio)
  f0.detect_f0s()
  notes = f0.detector.temporal_merge(me.groupings)
  for note in notes:
    print("-------------")
    print(note)
    print("\n\n")
