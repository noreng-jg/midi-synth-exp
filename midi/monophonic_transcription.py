from .f0 import F0Estimation, Crepe, PYin
from .meter_estimation import MeterEstimation, Audio

class MonophonicTranscription:
  notes = None
  audio = None

  def __init__(self, method, filename, metric):
    audio = Audio(filename)

    self.audio = audio

    f0 = F0Estimation(method, audio)
    me = MeterEstimation(audio)
    f0.detect_f0s()
    self.notes = f0.detector.raw_temporal_merge(me.groupings)
      
if __name__ == "__main__":
  mt = MonophonicTranscription("pyin", "dataset/corpus/6.wav")
