import librosa

class Audio:
  def __init__(self, filename: str):
    x, sr = librosa.load(filename)
    self.filename = filename
    self.signal = x
    self.sr = sr

  def __str__(self):
    return self.filename


if __name__ == "__main__":
  audio = Audio('../dataset/corpus/1.wav')
