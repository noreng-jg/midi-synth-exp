import librosa
import numpy as np
from .audio import Audio

class MeterEstimation:
  def __init__(self, audio): 
    self.onset_samples = librosa.onset.onset_detect(
        y = audio.signal,
        sr = audio.sr,
        units = 'samples'
    )

    self.onset_boundaries = np.concatenate(
        [
          [0],
          self.onset_samples,
          [len(audio.signal)]
        ]
    )

    self.onset_times = librosa.samples_to_time(self.onset_boundaries, sr = audio.sr)

    self.groupings = [
      [self.onset_times[i], self.onset_times[i+1]] for i in range(0 , len(self.onset_times) - 1)
    ]

if __name__ == "__main__":
  a = Audio('../dataset/corpus/1.wav')
  me = MeterEstimation(a)
